# GoogleOffice

Google Office es un proyecto personal, en el que todo el mundo puede contribuír si así lo desea, con el objetivo de crear una aplicación de escritorio basada en Electron con la que utilizar la suite de ofimática de Google directamente desde el escritorio.

## Características iniciales

Se podrán abrir documentos de ofimática con formatos libres o privativos. Buscando que todo el mundo pueda utilizar Google Docs, Spreadsheet o Slides de una forma sencilla y sin tener que abrir el navegador.

También se podrán guardar documentos en el escritorio directamente desde la aplicacion. Para tenerlos de forma local y poder después enviarselos a otra persona sin problema alguno

# Agredecimientos
* **maximegris** : [Repository](https://github.com/maximegris/angular-electron)
